Important!

The drush_git module is currently under development and is not in release state.  So if you run the Git commands, expect errors.  I have been asked to put this up so other developers could take a look and offer their suggestions.  Hopefully some of you will see it's usefulness like we do and help it grow.  This is a personal project so it often gets development delays due to clients needs.  Let us know if you are interested in co-maintaining or testing it out.

What is it?

The drush_git module currently provides a Git core API layer for Drush modules that use Git functionality and some Git deployment commands we are developing for our own organizational deployment needs, that utilize Git submodules.  I am thinking that the Git deployment commands will probably get separated in the future into a drush_git_deploy? (not sure on the name) module.  So basically there would be a Git API module with no Drush commands, and the Git deployment module that used the API module.

I am somewhat aware of other efforts to integrate Git into Drush through drush_make.  While I like this project, I wanted a CLI driven build process and the ability to have complete versioning of the Drupal projects.  As I understand it, drush_make fetches Git repos, this module creates them from Drupal packages.  Correct me if I am wrong here.  Read on to see the general architectural considerations.

Why would you need this module?

We don't use CVS (except for Drupal).  We much prefer the distributed model of development fostered by Git.  Git was made to handle the Linux kernel so it is tested on mission critical apps.  Creating repositories is easy (no server needed), making branches is easier, and merging branches is actually something you don't mind doing.  It basically makes development easier, safer, and much more collaborative in nature.  This is especially true when you use GitHub.

So how do you work with Drupal when you decide that you don't want to manage your projects through CVS?  That is an issue we have been dealing with.  Since Drupals project catalog is CVS repos and project releases, we need a way of taking these external directory structures and turning them into something we can work with, namely Git repositories.  We also want the Drupal branching structure of the releases included so we can check out differences between them.  This can help resolve issues that crop up.

If you need a way to build Drupal sites using Git SCM then this module is for you.  I have chosen to create a git command namespace for Git commands instead of relying on the traditional Drush commands, such as dl, to keep the command structure somewhat familiar to Git users (who this module is for).  I also did not have enough ways of hooking into Drush's native package management system to implement what we needed internally through the core package management commands.

General architecture.

The first release of this module will contain 6 commands:

1. drush git base init   <- initialize a feeder repository. (explained shortly)
2. drush git site init   <- initializes a Drupal site from a feeder repository.
3. drush git site clone  <- clones an existing Drupal site.
4. drush git add         <- add a new project to the feeder repo or Drupal site.
5. drush git rm          <- remove a project from a feeder repo or Drupal site.
6. drush git update      <- update all projects (including core)

It also contains some general purpose libraries (static method classes):

1. GitCore               <- basic reusable Git operations built off of Git core.
2. DiskUtils             <- file system interface
3. XMLUtils              <- XML utilities

At the core of the module, you have the classes:

1. Project           <- Drupal project (can be anything the Drupal PMS allows)

1.1 Base             <- Base repo Drupal site (NOT meant to be connected to DB)
                        (extends Project and contains Project instances)
                        
1.2 Site             <- Drupal site (meant to be connected to DB)
                        (extends Project and contains Project instances)

First of all, you will notice the distinction between something called a base and a site.  A "base" is a Git feeder repository that serves site repositories.  So sites are cloned from base repos or other site repos.  Base repos are not fully functioning Drupal sites, as they have no database connection.  What base repositories allow for is a gateway layer between Drupal's package management system and our deployed site code bases.

The system of having feeder (base) repositories has two advantages for us.  First, we can make changes to the packaged code at the feeder level and have that flow into our site upgrades more seamlessly.  We can also prepackage libraries into the base repo as needed by projects.  Secondly, we can do the Drupal package management system core and project upgrades on a non-live site and only pull the changes downstream if we successfully updated the base repo.  And if it fails, have no fear.  Pre-update backups are saved and restored if the upgrade failed, so you get more safety and more convienience in this process.  How many times have you regretted that a new version of core just came out? This can be a real problem if you manage a lot of sites.

One other consideration is going into the design of this module.  Git submodules are awesome.  Every project in this deployment system is a separate repository with a separate history.  This is accomplished by creating Git submodule based repositories for the Drupal sites.  This approach allows for the most flexibility in development of the projects.  But many people aren't familiar with their usage, so this module tries to abstract out their usage to the command structure as possible.  It also makes it a heck of a lot easier for those of us who are familiar with their usage (and peculiarities).  This is the goal at least.

What do you do now?

That is a good question.  One that has kept philosophers awake at night for a long time.  Well I can tell you what NOT to do.  Please don't use this on production systems with data and code you care about right now.  Not all of the commands are finished and I am still testing the commands that are finished.  If you are interested in exploring the code and maybe chipping in, then I welcome it.  That's why I put it up (aside from the fact that I have someone scolding me for not putting it up yet).  Beware the code needs to be cleaned up.  Feel free to contact me about it.  Submit your suggestions.  I am all ears, I just might not be all fingers if your suggestion doesn't suit our intended project purpose.

Adrian Webb ( adrian@collectivecolors.com )
CollectiveColors.com 
   